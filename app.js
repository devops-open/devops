const express = require('express')
const app = express()
const port = 3001
const serverName = "Server 1"
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {

    body = `<h1>Aplicativo rodando em ${serverName}</h1>`

    name = req.header("ngrok-auth-user-name")
    if (typeof name !== 'undefined') {
        body += 'Saudações ' + name
        body += '<br />'
    }

    token = req.header("ngrok-auth-oauth-access-token")
    if (typeof token !== 'undefined') {
        body += "Aqui está seu token: " + token
        body += '<br /><br />'

        if (token.indexOf('.') >= 0) {
            // this is a JWT
            const base64String = token.split('.')[1];
            const decodedValue = JSON.parse(Buffer.from(base64String, 'base64').toString('ascii'));
            body += "aqui ele está decodificado: <pre>" + JSON.stringify(decodedValue, null, 2)

        } else {
            body += "e isso não é um JWT"
        }
    }
})