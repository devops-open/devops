# Use a imagem base Python
FROM python:3.9

# Defina o diretório de trabalho na imagem
WORKDIR /app

# Copie o arquivo requirements.txt para o diretório de trabalho
COPY  C:\Users\André Luiz\Documents\Visual Code Projects\devops /app/

# Instale as dependências da aplicação
RUN pip install -r Lista.py

# Copie o restante do código-fonte da aplicação para o diretório de trabalho
COPY . .

# Especifique o comando para iniciar a aplicação (substitua 'app.py' pelo seu arquivo de entrada)
CMD [ "python", "Lista.py" ]
