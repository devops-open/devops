class ToDoApp:
    def __init__(self):
        self.tasks = []

    def create_task(self, task):
        self.tasks.append(task)
        print("Tarefa criada:", task)

    def read_tasks(self):
        if not self.tasks:
            print("A lista de tarefas está vazia.")
        else:
            print("Lista de tarefas:")
            for i, task in enumerate(self.tasks, start=1):
                print(f"{i}. {task}")

    def update_task(self, index, new_task):
        if 1 <= index <= len(self.tasks):
            self.tasks[index - 1] = new_task
            print("Tarefa atualizada.")

    def delete_task(self, index):
        if 1 <= index <= len(self.tasks):
            deleted_task = self.tasks.pop(index - 1)
            print("Tarefa deletada:", deleted_task)

def main():
    app = ToDoApp()

    while True:
        print("\nEscolha uma opção:")
        print("1. Criar tarefa")
        print("2. Ver tarefas")
        print("3. Atualizar tarefa")
        print("4. Deletar tarefa")
        print("5. Sair")

        choice = input("Opção: ")

        if choice == "1":
            task = input("Digite a nova tarefa: ")
            app.create_task(task)
        elif choice == "2":
            app.read_tasks()
        elif choice == "3":
            index = int(input("Digite o número da tarefa a ser atualizada: "))
            new_task = input("Digite a nova tarefa: ")
            app.update_task(index, new_task)
        elif choice == "4":
            index = int(input("Digite o número da tarefa a ser deletada: "))
            app.delete_task(index)
        elif choice == "5":
            print("Saindo...")
            break
        else:
            print("Opção inválida. Tente novamente.")

if __name__ == "__main__":
    main()
